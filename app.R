library(shiny)
library(shinydashboard)
library(shinyjs)
library(DT)
library(dplyr)
library(devtools)
library(randomcoloR)
library(ggplot2)
library(plotly)
library(xfun)
library(stats)

source("./R/helper_functions.R", local = T)
source("./R/menugauche.R", local = T)
source("./pages/pages_def_home.R", local = T)
source("./pages/pages_def_old.R", local = T)

options(encoding = 'UTF-8')

style <- tags$style(HTML(readLines("www/added_styles.css")) )
UI <- dashboardPage(
  skin = "blue",
  dashboardHeader(title = "RMosquito"),
  dashboardSidebar(MenuGauche),
  dashboardBody(
    
    shinyjs::useShinyjs(),
    #extendShinyjs(text = jscode),
    tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "bootstrap.min.readable.css")) ,
    tags$head(style),
    tags$head(tags$script(src = "message-handler.js")),
    tabItems(
      tabItem(tabName = "Home",         tabHome),
      tabItem(tabName = "Manual",         tabOld)
    )
  )
)


server <- function( input, output, session) {
  
  timeMaxVisible <<- 0
  timeMinVisible <<- 0
  
  
  clusterNotPrint <<- c()
  
  source("./server/opt_home.R", local=T)
  source("./server/opt_old.R", local=TRUE)
}

shinyApp(ui = UI, server = server)