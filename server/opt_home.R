
# ===================================================================================

output$exportCluster <- downloadHandler(
  filename = function() {
    paste("result.csv", sep = "")
  },
  content = function(file) {
    
    clusters <- as.numeric(names(h_clusters))
  
    
    result <- RAWPOINT %>% filter(object %in% clusters)
    
    result$oldobject <- result$object
    
    for(c in c(1:max(h_clusters))) {
      clusters <- as.numeric(names(h_clusters[h_clusters == c]))
      for(id in clusters) {
        #change the id
        result <- result %>% mutate(object = replace(object, object == id, c))
      }
    }
    
    result <- result %>% arrange(object)
    
    result <- result %>% select(object, oldobject, time, XSplined, YSplined, ZSplined, VXSplined, VYSplined, VZSplined)

    write.csv(result, file, row.names = FALSE)
  }
)

# ===================================================================================

eventButtonCluster <- eventReactive(input$Clustering, {
  
  session$sendCustomMessage(type = 'start_gear', message = "start")
  
  tabMinMax <- RAWDATA %>% filter(time >= input$sizeMosquito)
  
  size2 <- nrow(tabMinMax)
  
  individu <- matrix(rep(999, size2*size2), nrow=size2, ncol = size2)
  rownames(individu) <- tabMinMax$id
  colnames(individu) <- tabMinMax$id
  
  for(i in 1:(size2-1)) {
    for(j in (i+1):size2) {
      
      m1 <- tabMinMax[i,]
      m2 <- tabMinMax[j,]
      
      dist <- round(distanceM2(m1, m2, isolate(input$timeMosquito)), digits = 4)
      
      individu[i,j] = dist
      individu[j,i] = dist
    }
  }
  
  JIMMY <<- individu
  
  d <- as.dist(individu)
  h <<- hclust(d)

  kMosquito <<- size2 - length(which((h$height < 999)==TRUE))
  
  h_clusters <<- cutree(h, k=kMosquito)
  
  
#DELETE SOUND
  
  mosquitoT <- dataM
  
  all_LL <- list()
  indexALL <- 1
  
  for(f in c(1:kMosquito)) {
    
    
    
    mosquitoCluster <- as.numeric(names(h_clusters[h_clusters==f]))
    
    MM <- data.frame(id = numeric(), time = numeric())
    
    for(mos in mosquitoCluster) {
      mosquito <-  filter(mosquitoT, id == mos)
      MM <- rbind(MM, mosquito)
    }
    
    mosquitoCluster <- unique((MM %>% arrange(time))$id)
    
    
    indexE <- 1
    indexA <- 1
    
    maxlongCluster <- c()
    
    currentL <- c()
    all_List <- list()
    lastM <- NULL
    for(mos in mosquitoCluster) {
      mosquito <- RAWDATA %>% filter(id==mos)
      
      if(indexE == 1) {
        currentL <- c(currentL, mos)
      } else {
        
        dist <- abs(lastM$max - mosquito$min)
        
        if(dist > isolate(input$timeMosquito)) {
          all_List[[indexA]] <- currentL
          currentL <- c(mos)
          indexA <- indexA + 1
          
        } else {
          
          a <- c(lastM$maxX, lastM$maxY, lastM$maxZ)
          b <- c(mosquito$minX, mosquito$minY, mosquito$minZ)
          distXYZ <- distance3D(a, b)
          
          if(distXYZ > isolate(input$maxDistanceMosquito)) {
            
            all_List[[indexA]] <- currentL
            currentL <- c(mos)
            indexA <- indexA + 1
            
          } else {
          
            currentL <- c(currentL, mos)
          }
        }
        
        
      }
      
      
      lastM <- mosquito
      
      indexE <- indexE + 1
    }
    
    all_List[[indexA]] <- currentL
    
    if(length(all_List) > 1) {
      
      indexSelect <- 1
      lastMaxdistance <- 0
      
      for(mos in c(1:length(all_List))) {
        
        LL <- all_List[[mos]]
        
        
        totaltime <- 0
        for(mt in LL) {
          mosquito <- mosquito <- RAWDATA %>% filter(id==mt)
          
          
          totaltime <- totaltime + (mosquito$max - mosquito$min)
          
        }
        
        if(totaltime >= isolate(input$maxRangeTimeMosquito)) {
          all_LL[[indexALL]] <- LL
          indexALL <- indexALL + 1
        }
        
        
        
      }
      
    } else {
      all_LL[[indexALL]] <- all_List[[1]]
      indexALL <- indexALL + 1
    }
    
    
    
    
  }
  
  cont <- TRUE
  
  numberL <- 1
  Nlist <- list() 
  while(cont) {
    
    if(length(all_LL)>=1) {
      
      maxC <- 0
      indexLL <- 1
      LLC <- NULL
      for(idd in c(1:length(all_LL))) {
        
        LL <- all_LL[[idd]]
        
        mosquito <- RAWDATA %>% filter(id==LL[1])
        
        
        if(mosquito$min >= maxC) {
          maxC <- mosquito$min
          indexLL <- idd
          LLC <- LL
        }
        
      }
      
      all_LL[[indexLL]] <- NULL
      
      Nlist[[numberL]] <- LLC
      numberL <- numberL + 1
      
    } else {
      cont <- FALSE
    }
    
  }
  
  all_LL <- rev(Nlist)
  
  uuid <- unlist(all_LL)
  
  cont <- TRUE
  new_list <- list()
  indexNL <- 1
  
  
  
  for(idd in c(1:length(all_LL))) {
    
    LL <- all_LL[[idd]]
    
    idL <- c(LL)
    
    size <- length(LL)
    
    lastM <- RAWDATA %>% filter(id==LL[size])
    
    cont <- TRUE
    
    lastM <- lastM
    while(cont) {
      cont <- FALSE
      for(idd2 in c(1:length(all_LL))) {
        
        if(idd2 != idd) {
          
          LL2 <- all_LL[[idd2]]
          mosquito <-  RAWDATA %>% filter(id==LL2[1])
          
          if(mosquito$min - lastM$max  >= 0) {
            if(mosquito$min - lastM$max <= 1) {
              
              
              
              a <- c(lastM$maxX, lastM$maxY, lastM$maxZ)
              b <- c(mosquito$minX, mosquito$minY, mosquito$minZ)
              distXYZ <- distance3D(a, b)
              
              if(distXYZ <= isolate(input$maxDistanceMosquito)) {
                
                idL <- c(idL, LL2)
                lastM <- RAWDATA %>% filter(id==LL2[length(LL2)])
                cont <- TRUE  
              }
            }
          }
          
        }
        
      }
      
    }
    
    new_list[[indexNL]] <- idL
    indexNL <- indexNL + 1
    
  }
  
  
  deja_vue <- c()
  new_list2 <- list()
  indexNL <- 1
  for(idd2 in c(1:length(new_list))) {
    LL <- new_list[[idd2]]
    if(all(LL %in% deja_vue)) {
      
    } else {
      deja_vue <- c(deja_vue, LL)
      new_list2[[indexNL]] <- LL
      indexNL <- indexNL + 1
    }
  }
  
  
  all_LL <- new_list2
  
  j_clusters <- c()
  for(a in c(1:length(all_LL))) {
    L <- all_LL[[a]]
    value <- rep(a, length(L))
    names(value) <- L
    j_clusters <- c(j_clusters, value)
  }
  
  kMosquito <<- length(all_LL)
  
  h_clusters <<- j_clusters
  
  
  #App

  apparition <<- matrix(rep(0, size2*size2), nrow=size2, ncol = size2)
  rownames(apparition) <<- tabMinMax$id
  colnames(apparition) <<- tabMinMax$id
  
  for(i in 1:size2) {
    for(j in 1:size2) {
      
      m1 <- tabMinMax[i,]
      m2 <- tabMinMax[j,]
      
      dist <- distanceApp(m1, m2, input$startF)
      
      apparition[i,j] <<- round(dist, digits = 4)
      apparition[j,i] <<- apparition[i,j] # mirror
      
    }
    
  }

  tabStart <<- as.numeric(rownames(which( apparition <= 0.4, arr.ind=T )))
  
  #Disp
  
  disp <<- matrix(rep(0, size2*size2), nrow=size2, ncol = size2)
  rownames(disp) <<- tabMinMax$id
  colnames(disp) <<- tabMinMax$id
  
  for(i in 1:size2) {
    for(j in 1:size2) {
      
      m1 <- tabMinMax[i,]
      m2 <- tabMinMax[j,]
      
      dist <- distanceDis(m1, m2, input$startF)
      
      disp[i,j] <<- round(dist, digits = 4)
    }
    
  }
  
  tabEnd <<- as.numeric(rownames(which( disp <= 0.4, arr.ind=T )))
  
  session$sendCustomMessage(type = 'stop_gear', message = "stop")
  
})


  output$plotClustering = renderPlotly ({ 
    
    eventButtonCluster()
  
    p <- plot_ly(source="clusterPlot")
    
    mosquitoT <- dataM
    
    possibility <- isolate(input$showCouplingPossibility)
    
    if(possibility) {
    
      for(mos in tabStart) {
        mosquito <-  filter(mosquitoT, id == mos)
        p <- p %>% add_segments(x = mosquito[1, 2], xend = mosquito[1, 2], y = 0, yend = max(RAWDATA$id), name = paste0("app:", mos), line = list(color = "#2980b9", dash = "dash", width = 3))
      
      }
      
      for(mos in tabEnd) {
        mosquito <-  filter(mosquitoT, id == mos)
        p <- p %>% add_segments(x = mosquito[2, 2], xend = mosquito[2, 2], y = 0, yend = max(RAWDATA$id), name = paste0("dis:", mos), line = list(color = "#c0392b", dash = "dash", width = 3))
      }
      
    }
    
    startC <- isolate(input$startCoupling)
    endC <- isolate(input$endCoupling)
    
    if(is.numeric(startC)) {
      if(startC > 0) {
        p <- p %>% add_segments(x = startC, xend = startC, y = 0, yend = max(RAWDATA$id), name = "start coupling", line = list(color = "#000000", dash = "dash", width = 3))
      }
    }
    
    if(is.numeric(endC)) {
      if(endC > 0) {
        p <- p %>% add_segments(x = endC, xend = endC, y = 0, yend = max(RAWDATA$id), name = "end coupling", line = list(color = "#000000", dash = "dash", width = 3))
      }
    }
    
    uuid = unique(mosquitoT$id)
    
    color_mosquito_cluster <<- randomColor(kMosquito)
    
    yIndex <- 1
    
    
  
    for(f in c(1:kMosquito)) {
    
      mosquitoCluster <- as.numeric(names(h_clusters[h_clusters==f]))
      
      #colorM <- color_mosquito_cluster[f]
      colorM <- randomColor()
      
      MM <- data.frame(id = numeric(), time = numeric())
      
      for(mos in mosquitoCluster) {
        mosquito <-  filter(mosquitoT, id == mos)
        MM <- rbind(MM, mosquito)
      }
      
      mosquitoCluster <- unique((MM %>% arrange(time))$id)
      
      startIndex <- 1
      
      lastM <- NULL
    
      for(mos in mosquitoCluster) {
        mosquito <-  filter(mosquitoT, id == mos)
        
        if(!is.null(lastM)) {
          if(mosquito[1,2] - lastM[2,2] > 2) {
            yIndex <- yIndex + 1
            colorM <- randomColor()
          }
        }
        
        lastM <- mosquito
        
        nameCluster <- paste0("Cl:", f, " - Id:", mos)
        
        
        
        p <- p %>% add_segments(x = mosquito[1, 2], y = yIndex, xend = mosquito[2, 2], yend = yIndex, name = nameCluster, line = list(color = colorM,width = 2)) 
        
        if(startIndex == 1) {
          p <- p %>% add_markers(x = mosquito[1, 2], y = yIndex, name = nameCluster,marker = list(color = "#000000",size = 16))
          p <- p %>% add_markers(x = mosquito[1, 2], y = yIndex, name = nameCluster, marker = list(color = colorM,size = 10))
        } else {
          
          p <- p %>% add_markers(x = mosquito[1, 2], y = yIndex, name = nameCluster, marker = list(color = colorM,size = 10))
        }
        
        p <- p %>% add_markers(x = mosquito[2, 2], y = yIndex, name = nameCluster, marker = list(color = colorM,size = 10))
        
        startIndex <- startIndex + 1
        yIndex <- yIndex + 1
      }
      
      yIndex <- yIndex + 3
    }
    
    p <- p %>% layout(showlegend = FALSE)
    p
  })

# ===================================================================================

#' Render raw data plotly
output$plotRawData = renderPlotly ({ 
  req(input$dataMosquito)
  
  session$sendCustomMessage(type = 'start_gear0', message = "start")
  
  path <- input$dataMosquito$datapath

  mosquitoT <- importDataMosquito(path)

  dataM <<- mosquitoT
  
  timeMaxVisible <<- max(dataM$time)
  timeMinVisible <<- min(dataM$time)
  
  color_mosquito <<- randomColor(nrow(mosquitoT)/2)

  uuid = unique(dataM$id)
  
  pathMosquitoTab <<- c()
  
  selectMosquitoTab <<- c()
  
  vertical_id <- c()

  
  p <- plot_ly(source = "rawPlot")
  
  if(length(uuid) > 2) {
    for(index in c(1:length(uuid))) {
      mosquito <-  filter(mosquitoT, id == uuid[index])
      
      nameM <- paste0("Id:", uuid[index])

      if( mosquito[2,2] - mosquito[1,2] >= input$sizeMosquito) {
      
        p <- p %>% add_segments(x = mosquito[1, 2], y = mosquito[1, 1], xend = mosquito[2, 2], yend = mosquito[2, 1], name = nameM, line = list(color = color_mosquito[uuid[index]],width = 2)) 
        
        p <- p %>% add_markers(x = mosquito[1, 2], y = mosquito[1, 1], name = nameM, marker = list(color = color_mosquito[uuid[index]],size = 10))
        p <- p %>% add_markers(x = mosquito[2, 2], y = mosquito[2, 1], name = nameM, marker = list(color = color_mosquito[uuid[index]],size = 10))
        
        if(uuid[index] %in% vertical_id) {
          p <- p %>% add_segments(x = mosquito[1, 2], xend = mosquito[1, 2], y = 0, yend = max(mosquitoT), name = nameM, line = list(color = color_mosquito[uuid[index]], dash = "dash", width = 1))
          p <- p %>% add_segments(x = mosquito[2, 2], xend = mosquito[2, 2], y = 0, yend = max(mosquitoT), name = nameM, line = list(color = color_mosquito[uuid[index]], dash = "dash", width = 1))
        }
      }
    }
  }
  
  session$sendCustomMessage(type = 'stop_gear0', message = "stop")
  
  p <- p %>% layout(showlegend = FALSE)
  p
  
})

# ===================================================================================
  
  drawPlotCluster <- function() {
    clusterIdNotPrint <- c()
    
    for(c in clusterNotPrint) {
      clusterIdNotPrint <- c(clusterIdNotPrint, h_clusters[as.character(c)])
    }
    
    output$plotClustering = renderPlotly ({ 
      
      
      eventButtonCluster()
      
      p <- plot_ly(source="clusterPlot")
      
      mosquitoT <- dataM
      
      possibility <- isolate(input$showCouplingPossibility)
      
      if(possibility) {
        
        for(mos in tabStart) {
          mosquito <-  filter(mosquitoT, id == mos)
          p <- p %>% add_segments(x = mosquito[1, 2], xend = mosquito[1, 2], y = 0, yend = max(RAWDATA$id), name = paste0("app:", mos), line = list(color = "#2980b9", dash = "dash", width = 3))
        }
        
        for(mos in tabEnd) {
          mosquito <-  filter(mosquitoT, id == mos)
          p <- p %>% add_segments(x = mosquito[2, 2], xend = mosquito[2, 2], y = 0, yend = max(RAWDATA$id), name = paste0("dis:", mos), line = list(color = "#c0392b", dash = "dash", width = 3))
        }
        
      }
      
      startC <- isolate(input$startCoupling)
      endC <- isolate(input$endCoupling)
      
      if(is.numeric(startC)) {
        if(startC > 0) {
          p <- p %>% add_segments(x = startC, xend = startC, y = 0, yend = max(RAWDATA$id), name = "start coupling", line = list(color = "#000000", dash = "dash", width = 3))
        }
      }
      
      if(is.numeric(endC)) {
        if(endC > 0) {
          p <- p %>% add_segments(x = endC, xend = endC, y = 0, yend = max(RAWDATA$id), name = "end coupling", line = list(color = "#000000", dash = "dash", width = 3))
        }
      }
      
      uuid = unique(mosquitoT$id)
      
      color_mosquito_cluster <<- randomColor(kMosquito*10)
      
      for(f in c(1:kMosquito)) {
        
        mosquitoCluster <- as.numeric(names(h_clusters[h_clusters==f]))
        
        colorM <- color_mosquito_cluster[f]
        
        
        MM <- data.frame(id = numeric(), time = numeric())
        
        for(mos in mosquitoCluster) {
          mosquito <-  filter(mosquitoT, id == mos)
          MM <- rbind(MM, mosquito)
        }
        
        mosquitoCluster <- (MM %>% arrange(time))$id
        
        
        startIndex <- 1
        
        
        
        for(mos in mosquitoCluster) {
          mosquito <-  filter(mosquitoT, id == mos)
          
          CC <<- clusterIdNotPrint
          
          if(f %in% clusterIdNotPrint) {
            p <- p %>% add_segments(x = mosquito[1, 2], y = mosquito[1, 1], xend = mosquito[2, 2], yend = mosquito[2, 1], name = mos, line = list(color = "#e4e4e4",width = 1)) 
          } else {
            p <- p %>% add_segments(x = mosquito[1, 2], y = mosquito[1, 1], xend = mosquito[2, 2], yend = mosquito[2, 1], name = mos, line = list(color = colorM,width = 2)) 
          }
          
          if(startIndex == 1) {
            p <- p %>% add_markers(x = mosquito[1, 2], y = mosquito[1, 1], marker = list(color = "#000000",size = 16))
          } else {
            
            if(f %in% clusterIdNotPrint) {
              p <- p %>% add_markers(x = mosquito[1, 2], y = mosquito[1, 1], marker = list(color = "#e4e4e4",size = 4))
            } else {
              p <- p %>% add_markers(x = mosquito[1, 2], y = mosquito[1, 1], marker = list(color = colorM,size = 10))
            }
          }
          
          if(f %in% clusterIdNotPrint) {
              p <- p %>% add_markers(x = mosquito[2, 2], y = mosquito[2, 1], marker = list(color = "#e4e4e4",size = 4))
          } else {
            p <- p %>% add_markers(x = mosquito[2, 2], y = mosquito[2, 1], marker = list(color = colorM,size = 10))
          }
          
          startIndex <- startIndex + 1
          
        }
        
        for(mos in c(1:(length(mosquitoCluster)-1))) {
          mosquito1 <- filter(mosquitoT, id == mosquitoCluster[mos])
          mosquito2 <- filter(mosquitoT, id == mosquitoCluster[mos+1])
          
          if(f %in% clusterIdNotPrint) {
            p <- p %>% add_segments(x = mosquito1[2, 2], y = mosquito1[2, 1], xend = mosquito2[1, 2], yend = mosquito2[1, 1], name = mos, line = list(color = "#e4e4e4",width = 1)) 
            
          } else {
            p <- p %>% add_segments(x = mosquito1[2, 2], y = mosquito1[2, 1], xend = mosquito2[1, 2], yend = mosquito2[1, 1], name = mos, line = list(color = colorM,width = 2)) 
            
          }
        }
        
      }
      
      p <- p %>% layout(showlegend = FALSE)
      p
      
    })
    
  }
  
# ===================================================================================
  observeEvent(input$showAllCluster, {
    
    clusterNotPrint <<- c()
    
    drawPlotCluster()
    
  })
  
  
  observeEvent(input$hideCluster, {
    
    drawPlotCluster()
  
  
  })

#' Observe plotly click event on rawPlot
obsB <- observe({
  s <- event_data("plotly_click", source = "rawPlot")
  
  if (length(s) == 0) {
    
  } else {
    
    uuid <- s$y

    path <- pathMosquitoTab
    
    born <- selectMosquitoTab
    
    if(max((dataM %>% filter(id == uuid))$time) == s$x) {
      if(uuid %in% born) {
        born <- born[!born %in% uuid]
      } else {
        born <- c(born, uuid)
      }
    } else {
      if(uuid %in% path) {
        path <- path[!path %in% uuid]
      } else {
        path <- c(path, uuid)
      }
    }
    
    pathMosquitoTab <<- path
    selectMosquitoTab <<- born
    
    
    FilterUUID <- c()
    
    uuid = unique(dataM$id)
    
    if(!is.null(path)) {
      listM <- c(path)
      
      findM <- dataM %>% filter(id %in% listM)
      
      maxM <- max(findM$time)
      
      newt_uuid <- c()
      
      if(length(uuid) > 2) {
        for(index in c(1:length(uuid))) {
          mosquito <-  filter(dataM, id == uuid[index])
          
          if(min(mosquito$time) >= maxM) {
            newt_uuid <- c(newt_uuid, index)
          }
        }
      }
      
      FilterUUID <- unique(c(newt_uuid, listM))
    
    }

    output$plotRawData = renderPlotly ({ 
      mosquitoT <- dataM
      
      uuid = unique(mosquitoT$id)
      
      p <- plot_ly(source = "rawPlot")
      
      vertical_id <- selectMosquitoTab
      
      if(length(uuid) > 2) {
        for(index in c(1:length(uuid))) {
          mosquito <-  filter(mosquitoT, id == uuid[index])
          
          nameM <- paste0("Id:", uuid[index])
          
          if( mosquito[2,2] - mosquito[1,2] >= input$sizeMosquito) {
            if(length(FilterUUID) > 0) {
              if(mosquito$id[1] %in% FilterUUID) {
                
                colorM <- color_mosquito[uuid[index]]
                  
                  if(mosquito$id[1] %in% listM) {
                    colorM <- "#000000"
                  }
              
                p <- p %>% add_segments(x = mosquito[1, 2], y = mosquito[1, 1], xend = mosquito[2, 2], yend = mosquito[2, 1], name = nameM, line = list(color = colorM,width = 3)) 
                
                p <- p %>% add_markers(x = mosquito[1, 2], y = mosquito[1, 1], name = nameM, marker = list(color = colorM,size = 10))
                p <- p %>% add_markers(x = mosquito[2, 2], y = mosquito[2, 1], name = nameM, marker = list(color = colorM,size = 10))
                
                if(uuid[index] %in% vertical_id) {
                  p <- p %>% add_segments(x = mosquito[1, 2], xend = mosquito[1, 2], y = 0, yend = max(mosquitoT), name = nameM, line = list(color = colorM, dash = "dash", width = 1))
                  p <- p %>% add_segments(x = mosquito[2, 2], xend = mosquito[2, 2], y = 0, yend = max(mosquitoT), name = nameM, line = list(color = colorM, dash = "dash", width = 1))
                }
                
              } else {
                
                p <- p %>% add_segments(x = mosquito[1, 2], y = mosquito[1, 1], xend = mosquito[2, 2], yend = mosquito[2, 1], name = nameM, line = list(color = "#e4e4e4",width = 2)) 
              }
            } else {
              p <- p %>% add_segments(x = mosquito[1, 2], y = mosquito[1, 1], xend = mosquito[2, 2], yend = mosquito[2, 1], name = nameM, line = list(color = color_mosquito[uuid[index]],width = 3)) 
              
              p <- p %>% add_markers(x = mosquito[1, 2], y = mosquito[1, 1], name = nameM, marker = list(color = color_mosquito[uuid[index]],size = 10))
              p <- p %>% add_markers(x = mosquito[2, 2], y = mosquito[2, 1], name = nameM, marker = list(color = color_mosquito[uuid[index]],size = 10))
              
              if(uuid[index] %in% vertical_id) {
                p <- p %>% add_segments(x = mosquito[1, 2], xend = mosquito[1, 2], y = 0, yend = max(mosquitoT), name = nameM, line = list(color = color_mosquito[uuid[index]], dash = "dash", width = 1))
                p <- p %>% add_segments(x = mosquito[2, 2], xend = mosquito[2, 2], y = 0, yend = max(mosquitoT), name = nameM, line = list(color = color_mosquito[uuid[index]], dash = "dash", width = 1))
                
              }
            }
          }
        }
      }
      
      p <- p %>% layout(showlegend = FALSE)
      p
    })
    
    
  }
})


